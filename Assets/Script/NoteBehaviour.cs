using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NoteBehaviour : MonoBehaviour
{
    public static NoteBehaviour instance;
    [SerializeField] GameObject noteHolder;
    [SerializeField] GameObject NotePrefab;
    [SerializeField] Transform ScrolledContent;
    private void Start()
    {
        instance = this;
    }
    public void CreateNotePrefab(Note note)
    {
        GameObject newNote = Instantiate(NotePrefab, noteHolder.transform);
        newNote.GetComponent<NoteOb>().notePrefabTitle.text = note.Title;
        newNote.GetComponent<NoteOb>().notePrefabDescription.text = note.Description;
        newNote.name = note.Uid;
    }
    public void ShowNotes(List<Note> noteList)
    {
        foreach (Note note in noteList)
        {
            CreateNotePrefab(note);
        }
    }
    public void ClearScrolledContent()
    {
        foreach (Transform child in ScrolledContent)
        {
            Destroy(child.gameObject);
        }
    }
}
