using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NoteOb : MonoBehaviour
{
     public TextMeshPro notePrefabTitle;
     public TextMeshPro notePrefabDescription;
    private static string CurrentNoteUid;
    public void Click()
    {
        Debug.Log("Clicked on " + gameObject.name);
        CurrentNoteUid = gameObject.name;
    }

    public static Note FindCurrent()
    {
        foreach (var t in Controller.instance.Uuser.userNotes)
        {
            if (t.Uid == CurrentNoteUid)
                return t;
        }
        return null;
    }
}
