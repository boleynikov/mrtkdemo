using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Proyecto26;
using Microsoft.MixedReality.Toolkit.Utilities;
using TMPro;
using System;

public class Controller : MonoBehaviour
{
    public static Controller instance;
    
    public const string RootUrl = "https://boleynikovproj-99fd4-default-rtdb.firebaseio.com";
    public const string AuthKey = "AIzaSyBfWWeIkRJI95R4ENcCL-R1qlNQXV-Zyys";

    public string idToken;
    public static string localId;
    public User Uuser;

    [SerializeField] GridObjectCollection grid;
    [SerializeField] TMP_InputField emailInput;
    [SerializeField] TMP_InputField passwordInput;
    [SerializeField] TMP_InputField titleInput;
    [SerializeField] TMP_InputField descriptionInput;

    private void Start()
    {
        instance = this;
        emailInput.text = "ol.bogdan13@gmail.com";
        passwordInput.text = "mamochki13";
    }

    
    public void SignInBtn()
    {
        SignInUser(emailInput.text, passwordInput.text);       
    }
    public void SignUpBtn()
    {
        SignUpUser(emailInput.text, passwordInput.text);
    }
    private void PostToDatabase()
    {
        RestClient.Put(RootUrl + "/" + localId + ".json?auth=" + idToken, Uuser);
    }
    private void SignUpUser(string email, string password)
    {
        string userData = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\",\"returnSecureToken\":true}";
        RestClient.Post<SignResponse>("https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=" + AuthKey, userData).Then(
            response =>
            {
                idToken = response.idToken;
                localId = response.localId;
                Uuser = new User();
                PostToDatabase();
            }).Catch(error =>
            {
                Debug.Log(error);
            });
    }
    private void SignInUser(string email, string password)
    {
        string userData = "{\"email\":\"" + email + "\",\"password\":\"" + password + "\",\"returnSecureToken\":true}";
        RestClient.Post<SignResponse>("https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=" + AuthKey, userData).Then(
            response =>
            {
                idToken = response.idToken;
                localId = response.localId;
                Debug.Log(response.localId);
                RetrieveFromDatabase(response.localId);
            }).Catch(error =>
            {
                Debug.Log(error);
            });
    }
    private void RetrieveFromDatabase(string id)
    {
        RestClient.Get<User>(RootUrl + "/" + id + ".json?auth=" + idToken).Then(response =>
        {
            Uuser = response;
            Debug.Log(Uuser);
            NoteBehaviour.instance.ShowNotes(Uuser.userNotes);
            UpdateCollection();
        });
    }
    public void CreateNote()
    {
        Note note = new Note(titleInput.text, descriptionInput.text);
        Uuser.AddNote(note);
        PostToDatabase();
        NoteBehaviour.instance.ClearScrolledContent();
        NoteBehaviour.instance.ShowNotes(Uuser.userNotes);
        UpdateCollection();
    }
    public void EditNote()
    {
        Note note = NoteOb.FindCurrent();
        note.Title = titleInput.text == default ? note.Title : titleInput.text;
        note.Description = descriptionInput.text == default ? note.Description : descriptionInput.text;
        note.Date = long.Parse(DateTime.UtcNow.ToString("yyyyMMddHHmmss")).ToString();
        Uuser.EditNote(note);
        PostToDatabase();
        NoteBehaviour.instance.ClearScrolledContent();
        NoteBehaviour.instance.ShowNotes(Uuser.userNotes);
        UpdateCollection();
    }
    public void UpdateCollection()
    {
        grid.UpdateCollection();
    }
    
}
